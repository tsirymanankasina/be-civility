
package civility.gov.app.entities.projection;

import org.springframework.data.rest.core.config.Projection;

import civility.gov.app.entities.Person;

@Projection(name = "person", types = Person.class)
public interface PersonProjection { // getters return value, only can be projected
  public long getAge();
}
