
package civility.gov.app.entities;

import java.time.LocalDate;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import civility.gov.app.utils.status.MarriageSituation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "marriage", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"marriageCertificateNumber"})})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Marriage extends RootAbstractEntity {

  private static final long  serialVersionUID = 5872433437601788111L;

  private Long               marriageCertificateNumber;

  @PastOrPresent
  private LocalDate          marriageDate;

  @Enumerated(EnumType.STRING)
  @NotNull
  private MarriageSituation  marriageSituation;

  /**
   * constraint on husband and wife will be managed by native sql script
   */
  @NotNull
  @OneToOne
  @JoinColumn(name = "person_husband_id")
  private Person             husband;

  @NotNull
  @OneToOne
  @JoinColumn(name = "person_wife_id")
  private Person             wife;

  /**
   * unidirectional many-to-many relationship, table "t_marriage_witness" will be created
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "t_marriage_witness",
          joinColumns = @JoinColumn(name = "marriage_id", referencedColumnName = "id"),
          inverseJoinColumns = @JoinColumn(name = "person_witness_id", referencedColumnName = "id"))
  private Collection<Person> witnesses;

  /**
   * unidirectional many-to-many relationship, table "t_marriage_signatory" will be created
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "t_marriage_signatory",
          joinColumns = @JoinColumn(name = "marriage_id", referencedColumnName = "id"),
          inverseJoinColumns = @JoinColumn(name = "person_signatory_id", referencedColumnName = "id"))
  private Collection<Person> signatories;

  private String             place;

  private String             description;
}
