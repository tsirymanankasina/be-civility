
package civility.gov.app.entities;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import civility.gov.app.utils.status.Sex;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "person", uniqueConstraints = {@UniqueConstraint(columnNames = {"registrationNumber"})})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person extends RootAbstractEntity {

  private static final long serialVersionUID = 8582433437601788991L;

  private Long              registrationNumber;

  private String            surename;

  @NotNull
  private String            givenNames;

  @NotNull
  @Enumerated(EnumType.STRING)
  private Sex               sex;

  @OneToOne(mappedBy = "person")
  private BirthInfo         birthInfo;

  @OneToOne(mappedBy = "person")
  private CivilStatus       civilStatus;

  @ManyToOne
  @JoinColumn(name = "father_id")
  private Person            father;

  @ManyToOne
  @JoinColumn(name = "mother_id")
  private Person            mother;

  @Positive
  @Max(value = 250)
  private short             bodySize;

  private String            eyeColor;

  private String            skinColor;

  @Email
  private String            email;

  private String            particularAssigned;

  /*
  Automatically interpreted by Spring data rest as a getter by using keyword "get".
  Projection not nessecary for that.
  Note: if you call the function like getAges(), "ages" attribute will be projected
   */
  public Long getAge() {
    if(this.birthInfo != null && this.birthInfo.getBirthDate() != null)
      return this.birthInfo.getBirthDate().until(LocalDateTime.now(), ChronoUnit.YEARS);
    return null;
  }

  public Boolean isOfLegalAge() {
     return this.getAge() != null  ? this.getAge() > 17 : null;
  }
}
