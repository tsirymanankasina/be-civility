
package civility.gov.app.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.PastOrPresent;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "birth_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BirthInfo implements Serializable {
  /**
   * NOT AUTO GeneratedValue
   */
  @Id
  private UUID person_id;
  /*
  * PK and FK
  * */
  @OneToOne
  @MapsId
  @JoinColumn(name = "person_id")
  private Person person;

  @PastOrPresent
  private LocalDateTime birthDate;

  private String place;

  private String description;
}
