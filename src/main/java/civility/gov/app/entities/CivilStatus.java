
package civility.gov.app.entities;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "civil_status", uniqueConstraints = {@UniqueConstraint(columnNames = {"civilStatusNumber"})})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CivilStatus extends RootAbstractEntity {

  private static final long  serialVersionUID = 1234433437601788111L;

  @NotNull
  private Long               civilStatusNumber;

  @OneToOne
  @JoinColumn(name = "person_id") // foreign key
  private Person             person;

  private LocalDateTime      actDate;

  /**
   * unidirectional many-to-many relationship table "t_civil_status_witness" will be created
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "t_civil_status_witness",
          joinColumns = @JoinColumn(name = "civil_status_id", referencedColumnName = "id"),
          inverseJoinColumns = @JoinColumn(name = "person_witness_id", referencedColumnName = "id"))
  private Collection<Person> witnesses        = Collections.emptySet();

  /**
   * unidirectional many-to-many relationship table "t_civil_status_signatory" will be created
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "t_civil_status_signatory",
          joinColumns = @JoinColumn(name = "civil_status_id", referencedColumnName = "id"),
          inverseJoinColumns = @JoinColumn(name = "person_signatory_id", referencedColumnName = "id"))
  private Collection<Person> signatories      = Collections.emptySet();

  private String             description;
}
