
package civility.gov.app.entities;

import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.PastOrPresent;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "death", uniqueConstraints = {@UniqueConstraint(columnNames = {"deathRecordNumber"})})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Death extends RootAbstractEntity {

  private static final long  serialVersionUID = 1473433437601788111L;

  private Long               deathRecordNumber;

  @OneToOne
  @JoinColumn(name = "person_id")
  private Person             person;

  @PastOrPresent
  private LocalDateTime      deathDate;

  /**
   * unidirectional many-to-many relationship, table "t_death_record_witness" will be created
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "t_death_record_witness",
          joinColumns = @JoinColumn(name = "death_record_id", referencedColumnName = "id"),
          inverseJoinColumns = @JoinColumn(name = "person_witness_id", referencedColumnName = "id"))
  private Collection<Person> witnesses;

  /**
   * unidirectional many-to-many relationship, table "t_death_record_signatory" will be created
   */
  @ManyToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "t_death_record_signatory", joinColumns = @JoinColumn(name = "death_record_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "person_signatory_id", referencedColumnName = "id"))
  private Collection<Person> signatories;

  private String             place;

  private String             description;
}
