
package civility.gov.app.entities;


import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "national_id_card", uniqueConstraints = {@UniqueConstraint(columnNames = {"identityNumber"})})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NationalIDCard extends RootAbstractEntity {

  private static final long serialVersionUID = 7982433437601788111L;

  @OneToOne
  @JoinColumn(name = "person_id") // foreign key
  private Person            person;

  @NotNull
  private Long              identityNumber;

  private LocalDate         issueDate;

  private LocalDate         expirationDate;

  @NotNull
  private String            serialNumber;
  // duplicata ??
  // Lieu de délivrance ?? dépend de la classe pour l'emplacement
}
