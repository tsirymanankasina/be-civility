
package civility.gov.app.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import lombok.Data;

@MappedSuperclass
@Data
public abstract class RootAbstractEntity implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  private LocalDateTime creationDate;

  private LocalDateTime lastUpdate;

  private String    status;

  @PrePersist
  public void setCreationDate() {
    if (null == this.getCreationDate()) {
      this.creationDate = LocalDateTime.now();
    }

    if (null == this.getLastUpdate()) {
      this.lastUpdate = LocalDateTime.now();
    }
  }

  @PreUpdate
  public void setLastUpdate() {
    if (null == this.getLastUpdate()) {
      this.lastUpdate = LocalDateTime.now();
    }
  }
}
