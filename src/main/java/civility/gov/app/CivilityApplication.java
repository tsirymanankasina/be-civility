
package civility.gov.app;

import civility.gov.app.entities.BirthInfo;
import civility.gov.app.repository.BirthInfoRepository;
import civility.gov.app.utils.status.Sex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import civility.gov.app.entities.Person;
import civility.gov.app.repository.PersonRepository;

import java.time.LocalDateTime;

@SpringBootApplication
public class CivilityApplication implements CommandLineRunner {

  @Autowired
  PersonRepository personRepository;
  @Autowired
  BirthInfoRepository birthInfoRepository;

  public static void main(String[] args) {
    SpringApplication.run(CivilityApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    Person mahindra = new Person();
    mahindra.setGivenNames("Raza");
    mahindra.setSurename("Mahindra");
    mahindra.setSex(Sex.FEMALE);
    /*
    In java, integer literals are of type int by default.
    So 170 is an integer for java which we need to cast to short instead
     */
    mahindra.setBodySize((short)170);

    BirthInfo mahindraBirthInfo = new BirthInfo();
    mahindraBirthInfo.setBirthDate(LocalDateTime.parse("2001-11-11T18:34:50.63"));
    mahindraBirthInfo.setDescription("Test of birthInfo");
    mahindraBirthInfo.setPerson(mahindra); // instaed below
    // mahindra.setBirthInfo(mahindraBirthInfo);

    birthInfoRepository.save(mahindraBirthInfo);
    personRepository.save(mahindra);

  }
}
