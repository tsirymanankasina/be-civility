package civility.gov.app.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import civility.gov.app.entities.NationalIDCard;

@RepositoryRestResource(collectionResourceRel = "nationalidcards", path = "nationalidcards")
public interface NationalIDCardRepository extends JpaRepository<NationalIDCard, UUID> {
}
