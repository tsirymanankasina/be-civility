
package civility.gov.app.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import civility.gov.app.entities.Marriage;

@RepositoryRestResource(collectionResourceRel = "marriages", path = "marriages")
public interface MarriageRepository extends JpaRepository<Marriage, UUID> {
}
