
package civility.gov.app.repository;

import civility.gov.app.entities.CivilStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(collectionResourceRel = "civilstatus", path = "civilstatus")
public interface CivilStatusRepository extends JpaRepository<CivilStatus, UUID> {
}
