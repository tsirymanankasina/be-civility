
package civility.gov.app.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import civility.gov.app.entities.Death;

@RepositoryRestResource(collectionResourceRel = "deaths", path = "deaths")
public interface DeathRepository extends JpaRepository<Death, UUID> {
}
