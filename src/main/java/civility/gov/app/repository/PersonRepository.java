
package civility.gov.app.repository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import civility.gov.app.entities.Person;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;

@RepositoryRestResource(collectionResourceRel = "persons", path = "persons")
public interface PersonRepository extends JpaRepository<Person, UUID> {

    // TODO LocalDateTime parsing
    @RestResource(path = "/byDateOfBirthBetween")
    Collection<Person> findByBirthInfo_BirthDateBetween(@DateTimeFormat(iso = DateTimeFormat.ISO.TIME) LocalDateTime start,
                                                        @DateTimeFormat(iso = DateTimeFormat.ISO.TIME) LocalDateTime end);
}
