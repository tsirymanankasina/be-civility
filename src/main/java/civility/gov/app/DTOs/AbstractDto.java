
package civility.gov.app.DTOs;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractDto implements Serializable {

  private static final long serialVersionUID = 5432433498701788111L;

  private Long              id;

  private LocalDate         creationDate;

  private LocalDate         deletionDate;

  private LocalDate         lastUpdate;

  private String            status;
}
