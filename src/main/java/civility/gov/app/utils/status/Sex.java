package civility.gov.app.utils.status;

public enum Sex {
    MALE, FEMALE;
}
