**Using Spring data rest :**

1. http://localhost:8080/api/civility
2. http://localhost:8080/api/civility/persons/search/byDateOfBirthBetween?start=***&end=***


**ManyToMany DOCs :**
* https://koor.fr/Java/TutorialJEE/jee_jpa_many_to_many.wp
* https://howtoprogramwithjava.com/hibernate-manytomany-unidirectional-bidirectional/

**Install "uuid-ossp" extension for postgresql before using "uuid_generate_v4()" function**
* CREATE EXTENSION IF NOT EXISTS "uuid-ossp"
* https://webdevdesigner.com/q/extension-exists-but-uuid-generate-v4-fails-123608/

**Youssfi link videos:**
* https://www.youtube.com/watch?v=O7D6hFg1P3I  [RepositoryRestResource, Projection]
